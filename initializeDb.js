const knex = require('knex');
const { CONNECTION_STRING } = require('./config');
const pg = knex({
    client: 'pg',
    connection: CONNECTION_STRING
  });

module.exports = pg;