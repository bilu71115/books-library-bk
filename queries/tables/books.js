module.exports = {
    up:  async function (pg) {
        if(await pg.schema.hasTable("books")){
               return null;
        }
        return Promise.all([
            pg.schema.createTable("books", function (table) {
                table.increments(); 
                table
                .integer("studentId")
                .references("id")
                .inTable("students")
                .onDelete("RESTRICT");
                table.string('bookName');
                table.string('author');
                table.timestamp('dateOfBorrow');
                table.timestamp('expectedDateOfReturn');
            }).then(function () {
                console.log("Books Table Created");
            }
            ),
        ]);
    },
    down: function (pg) {
        return Promise.all([
            pg.schema.dropTableIfExists("books")
        ]);
    }
}