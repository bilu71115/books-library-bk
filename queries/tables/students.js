module.exports = {
    up: async function (pg) {
        if(await pg.schema.hasTable("students")){
            return null;
        }
        return Promise.all([
            pg.schema.createTable("students", function (table) {
                table.increments();
                table.string('firstName');
                table.string('lastName');
            }).then(function () {
                console.log("Students Table Created");
            }
            ),
        ]);
    },
    down: function (pg) {
        return Promise.all([
            pg.schema.dropTableIfExists("students")
        ]);
    }
}