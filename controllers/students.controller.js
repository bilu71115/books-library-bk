module.exports = {
    addStudent: (pg) => async function (req, res) {
        try {
            const { firstName, lastName } = req.body;
            await pg("students").insert({
                firstName,
                lastName
            });
            res.status(200).json({ message: "Student Successfully Created!" });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    getStudent: (pg) => async function (req, res) {
        try {
            const id = req.params.id;
            const students = await pg.select('*')
                .from('students')
                .where('students.id', id);
            res.status(200).json({ students });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    getStudents: (pg) => async function (req, res) {
        try {
            const students = await pg.select('*').from('students');
            res.status(200).json({ students });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    updateStudent: (pg) => async function (req, res) {
        try {
            const { id } = req.params;
            const { firstName, lastName } = req.body;
            await pg("students").update({ firstName, lastName }).where("students.id", id);
            res.status(200).json({ message: "Student Successfully Updated!" });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    deleteStudent: (pg) => async function (req, res) {
        try {
            const id = req.params.id;
            await pg("students").del().where("id", id);
            res.status(200).json({ message: "Student Successfully Deleted!" });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    }, 
}