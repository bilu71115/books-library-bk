module.exports = {
    addBook: (pg) => async function (req, res) {
        try {
            const { bookName, author } = req.body;
            await pg("books").insert({
                bookName,
                author
            });
            res.status(200).json({ message: "Book Successfully Created!" });
        }
        catch (err) {
            
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    getBook: (pg) => async function (req, res) {
        try {
            const id = req.params.id;
            const books = await pg.select(['books.author', 'books.id', 'books.bookName',
                'books.studentId', 'books.dateOfBorrow', 'books.expectedDateOfReturn',
                'student.firstName', 'student.lastName'])
                .from('books')
                .leftJoin('students as student', 'books.studentId', '=', 'student.id')
                .where('books.id', id);
            res.status(200).json({ books });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    getBooks: (pg) => async function (req, res) {
        try {
            const books = await pg.select(['books.author', 'books.id', 'books.bookName',
                'books.studentId', 'books.dateOfBorrow', 'books.expectedDateOfReturn',
                'student.firstName', 'student.lastName'])
                .from('books')
                .leftJoin('students as student', 'books.studentId', '=', 'student.id');
            res.status(200).json({ books });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    updateBook: (pg) => async function (req, res) {
        try {
            const id = req.params.id;
            const { author, bookName } = req.body;
            await pg("books").update({ author, bookName }).where("id", id);
            res.status(200).json({ message: "Book Successfully Updated!" });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    deleteBook: (pg) => async function (req, res) {
        try {
            const id = req.params.id;
            await pg("books").del().where("id", id);
            res.status(200).json({ message: "Book Successfully Deleted!" });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    }, 
    bookBorrowed: (pg) => async function (req, res) {
        try {
            if (req.body.studentId && await pg.select('*').from('students').where("id", req.body.studentId)) {
                const id = req.params.id;
                const { studentId } = req.body;
                const dateOfBorrow = new Date().toISOString();
                const expectedDateOfReturn = new Date(req.body.expectedDateOfReturn).toISOString();
                await pg("books").update({ studentId, dateOfBorrow, expectedDateOfReturn }).where("books.id", id);
                res.status(200).json({ message: "Book Successfully Borrowed!" });
            }
            else {
                res.status(404).json({ message: "Provided ID for student is not valid." });
            }
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    },
    bookReturned: (pg) => async function (req, res) {
        try {
            const id = req.params.id;
            await pg("books").update({ studentId: null, dateOfBorrow: null, expectedDateOfReturn: null }).where("books.id", id);
            res.status(200).json({ message: "Book Successfully Returned!" });
        }
        catch (err) {
            res.status(500).json({ message: "Something went wrong, try again!" });
        }
    }
}