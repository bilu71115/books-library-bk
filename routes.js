const booksControllers = require("./controllers/books.controllers");
const studentsController = require("./controllers/students.controller");

module.exports = function (app, pg) {
    // Books Routes
    app.post("/books", booksControllers.addBook(pg));
    app.get("/books", booksControllers.getBooks(pg));
    app.put("/books/:id", booksControllers.updateBook(pg));
    app.post("/borrowBook/:id", booksControllers.bookBorrowed(pg));
    app.delete("/borrowBook/:id", booksControllers.bookReturned(pg));
    app.get("/books/:id", booksControllers.getBook(pg));
    app.delete("/books/:id", booksControllers.deleteBook(pg));

    // Students Routes
    app.post("/students", studentsController.addStudent(pg));
    app.get("/students", studentsController.getStudents(pg));
    app.put("/students/:id", studentsController.updateStudent(pg));
    app.get("/students/:id", studentsController.getStudent(pg));
    app.delete("/students/:id", studentsController.deleteStudent(pg));
};