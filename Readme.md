# Books Library Backend
Set the `./config.js` file according to your requirements. 
Start the project by running the command `npm start`

Important:- You can't delete a Student whom a book is assigned. Delete or remove borrower from that book first.