const pg = require("./initializeDb");
const express = require("express");
const students = require("./queries/tables/students");
const books = require("./queries/tables/books");
const cors = require("cors");
const { PORT } = require("./config");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

require("./routes")(app, pg);

app.listen(PORT || 8080, async ()=>{
    console.log(`App is running fine.`);
    await students.up(pg);
    await books.up(pg);
    
    //Maybe useful if you want to drop tables
    // await books.down(pg);
    // await students.down(pg);
})